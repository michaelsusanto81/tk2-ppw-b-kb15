from django.test import TestCase, Client
from django.urls import resolve
from .views import add_drink, get_all_drinks, show_all_drinks
from .models import Minuman
from .forms import FormAddDrink


class AddDrinkUnitTest(TestCase):
    def test_url_is_exist(self):
        response = Client().get("/addDrink/")
        self.assertEqual(response.status_code, 302)

    def test_addDrink_using_right_function(self):
        found = resolve('/addDrink/')
        self.assertEqual(found.func, add_drink)

    def test_getAllDrinks_using_right_function(self):
        found = resolve('/addDrink/allDrinks/getAllDrinks/')
        self.assertEqual(found.func, get_all_drinks)

    def test_allDrinks_using_right_function(self):
        found = resolve('/addDrink/allDrinks/')
        self.assertEqual(found.func, show_all_drinks)

    # def test_addDrink_using_right_template(self):
    #     response = Client().get("/addDrink/")
    #     self.assertTemplateUsed(response, "addDrink/add_drink.html")

    def test_model_can_add_new_drink(self):
        #Add a new drink
        new_drink = Minuman.objects.create(
            drink_name= "Teh", category= "Tea", flavor= "Chocolate",
            sugar_rate= '0%', hot_or_ice = "Hot", blend_or_brew="Blend",
            store_name="Kedai Christo", price=15000,
        )
        count_drink = Minuman.objects.all().count()
        self.assertEqual(count_drink, 1)

        all_obj = Minuman.objects.all()
        obj = all_obj[0]

        #test if field name is exist
        self.assertEqual(obj._meta.get_field('drink_name').verbose_name, "drink name")
        self.assertEqual(obj._meta.get_field("category").verbose_name, "category")
        self.assertEqual(obj._meta.get_field("flavor").verbose_name, "flavor")
        self.assertEqual(obj._meta.get_field("sugar_rate").verbose_name, "sugar rate")
        self.assertEqual(obj._meta.get_field("hot_or_ice").verbose_name, "hot or ice")
        self.assertEqual(obj._meta.get_field("blend_or_brew").verbose_name, "blend or brew")
        self.assertEqual(obj._meta.get_field("store_name").verbose_name, "store name")
        self.assertEqual(obj._meta.get_field("price").verbose_name, "price")

    '''Test Form'''
    def test_form_field_exist(self):
        form = FormAddDrink()
        self.assertIn('id="id_drink_name', form.as_p())
        self.assertIn('id="id_category', form.as_p())
        self.assertIn('id="id_flavor', form.as_p())
        self.assertIn('id="id_sugar_rate', form.as_p())
        self.assertIn('id="id_hot_or_ice', form.as_p())
        self.assertIn('id="id_blend_or_brew', form.as_p())
        self.assertIn('id="id_store_name', form.as_p())
        self.assertIn('id="id_store_location', form.as_p())
        self.assertIn('id="id_price', form.as_p())
