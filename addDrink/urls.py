from django.conf.urls import url
from django.urls import path
from . import views

app_name = "addDrink"

urlpatterns = [
    path('', views.add_drink, name="add_drink"),
    path('allDrinks/getAllDrinks/', views.get_all_drinks, name="get_drink"),
    path('allDrinks/', views.show_all_drinks, name="all_drink")
]