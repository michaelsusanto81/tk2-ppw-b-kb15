from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http.response import JsonResponse
from django.contrib.auth.decorators import login_required
from django.core import serializers
from . import models
from . import forms

""" Function for outputing add drink form """
@login_required(login_url='/')
def add_drink(request):
    form = forms.FormAddDrink()
    if request.method == "POST":
        form = forms.FormAddDrink(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('addDrink:add_drink')
    return render(request, 'addDrink/add_drink.html', {'form':form})

"""Function for send all drink data in JSON format"""
@login_required(login_url='/')
def get_all_drinks(request):
    all_drinks = models.Minuman.objects.all()
    return HttpResponse(serializers.serialize('json', all_drinks), 
        content_type= "text/json-comment-filtered")

"""Function for render template all drinks"""
@login_required(login_url='/')
def show_all_drinks(request):
    return render(request, 'addDrink/list_drink.html')