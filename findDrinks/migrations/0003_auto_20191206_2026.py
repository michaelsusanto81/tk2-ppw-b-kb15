# Generated by Django 3.0 on 2019-12-06 13:26

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('findDrinks', '0002_auto_20191206_2014'),
    ]

    operations = [
        migrations.DeleteModel(
            name='BlendOrBrew',
        ),
        migrations.DeleteModel(
            name='Category',
        ),
        migrations.DeleteModel(
            name='Flavor',
        ),
        migrations.DeleteModel(
            name='HotOrIce',
        ),
        migrations.DeleteModel(
            name='SugarRate',
        ),
    ]
