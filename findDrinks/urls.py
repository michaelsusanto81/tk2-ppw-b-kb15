from django.conf.urls import url
from django.contrib import admin
from django.urls import include,path
from findDrinks import views

app_name = "findDrinks"

urlpatterns = [
    path('findDrinks/', views.findDrinks, name='findDrinks'),
    path('logout/', views.logout, name='logout'),
    path('getRecommendation/<slug:category>/', views.getRecommendation, name='getRecommendation'),
]