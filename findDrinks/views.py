from django.shortcuts import render, redirect
from findDrinks.forms import UserRequestForm
from findDrinks.models import UserRequest
from addDrink.models import Minuman
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import login as user_login, logout as user_logout
from django.core import serializers
from django.http import HttpResponse, JsonResponse
# from django.conf import settings

# Create your views here.
@login_required(login_url= "/")
def findDrinks(request):
	form = UserRequestForm(request.POST or None)
	response = {'form':form}
	if(request.method == 'POST' and form.is_valid()):
		response['category'] = request.POST['category']
		response['flavor'] = request.POST['flavor']
		response['sugarRate'] = request.POST['sugarRate']
		response['hotOrIce'] = request.POST['hotOrIce']
		response['blendOrBrew'] = request.POST['blendOrBrew']
		response['userLocation'] = request.POST['userLocation']
		userReq = UserRequest(category=response['category'],
			flavor=response['flavor'],
			sugarRate=response['sugarRate'],
			hotOrIce=response['hotOrIce'],
			blendOrBrew=response['blendOrBrew'],
			userLocation=response['userLocation'])
		userReq.save()

		userReqs = Minuman.objects.filter(category=response['category'],
			flavor=response['flavor'],
			sugar_rate=response['sugarRate'],
			hot_or_ice=response['hotOrIce'],
			blend_or_brew=response['blendOrBrew'],
			store_location=response['userLocation'])
		if(userReqs.count() <= 0):
			userReqs = None
						
		response['userReqs'] = userReqs
	return render(request, 'findDrinks/findDrinks.html', response)

def logout(request):
	user_logout(request)
	return redirect('/')

def getRecommendation(request, category):
	userReq = Minuman.objects.filter(category=category).values()
	# base_url = settings.MEDIA_ROOT
	lst = list(userReq)
	# lst.insert(0, base_url)
	# print(JsonResponse(list(userReq), safe=False))
	# return HttpResponse(serializers.serialize('json',userReq), content_type="text/json-comment-filtered")
	return JsonResponse(lst, safe=False)