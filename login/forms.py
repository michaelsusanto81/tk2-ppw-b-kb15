from django import forms
from .models import Login

class LoginForm(forms.ModelForm):
	username=forms.CharField(
		widget=forms.TextInput(
			attrs={
				'required':True,
			}
		)
	)
	password = forms.CharField(
		widget= forms.PasswordInput(
			attrs = {
				'required' :True,
			}
		)
	)
	class Meta:
		model = Login
		fields = ('username', 'password')