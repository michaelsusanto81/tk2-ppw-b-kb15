from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.
class LoginUnitTest(TestCase):
	def test_login(self):
		c = Client()
		user = User.objects.create(username='michael')
		user.set_password('michael')
		user.save()
		data = {'username':'michael', 'password':'michael'}

		response = c.post('/', data=data)

		response2 = c.get('/')
		self.assertEqual(response2.status_code, 302)

		response3 = c.get('/findDrinks/')
		self.assertEqual(response3.status_code, 200)

		# test blank url after login
		response = c.get('/')
		self.assertEqual(response.status_code, 302)