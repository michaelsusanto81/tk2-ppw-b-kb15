from django.conf.urls import url
from django.contrib import admin
from django.urls import include,path
from . import views

app_name = "login"

urlpatterns = [
	path('', views.loginuser, name='loginpage'),
]