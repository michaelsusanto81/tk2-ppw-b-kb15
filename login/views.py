from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login

# Create your views here.
def loginuser(request):
	if request.user.is_superuser and request.user.is_authenticated:
		return redirect('/addDrink/')
	if request.user.is_authenticated:
		return redirect('/findDrinks/')
	if request.method == 'POST':
		form = AuthenticationForm(data=request.POST)
		if form.is_valid():
			user = form.get_user()
			login(request, user)
			if request.user.is_superuser:
				return redirect('/addDrink/')
			else:
				return redirect('/findDrinks/')
	else:
		form = AuthenticationForm()
	return render(request, 'login.html', {'form':form})