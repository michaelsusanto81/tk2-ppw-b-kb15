from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

class Register_Form(UserCreationForm):
	first_name = forms.CharField(required=True, widget=forms.TextInput(attrs={'autofocus':'autofocus'}))
	email = forms.EmailField(required=True)

	# password1 = forms.RegexField(
	# 			label="Password", regex=r'^(?=.*\W+).*$',
	# 			help_text="Min. 6 chars with at least 1 non-alphanumeric char", widget=forms.PasswordInput, 
	# 			min_length=6)
	password1 = forms.CharField(
				label="Password",
				help_text=None, widget=forms.PasswordInput, 
				min_length=8)
	password2 = forms.CharField(
				label="Password confirmation",
				help_text=None, widget=forms.PasswordInput, 
				min_length=8)
	class Meta:
		model = User
		fields = (
			'first_name',
			'last_name',
			'username',
			'email',
			'password1',
			'password2'
		)
		help_texts = {
			'username': None
		}
			

	def save(self, commit=True):
		user = super(Register_Form, self).save(commit=False)
		user.first_name = self.cleaned_data['first_name']
		user.last_name = self.cleaned_data['last_name']
		user.email = self.cleaned_data['email']

		if commit:
			user.save()

		return user