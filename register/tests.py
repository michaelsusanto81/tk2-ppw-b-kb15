from django.test import TestCase, Client
from django.urls import resolve
from register.views import register, check_username
from register.forms import Register_Form
from django.http import HttpRequest, JsonResponse
from django.contrib.auth.models import User

# Create your tests here.
class RegisterUnitTest(TestCase):

	def test_register_url_is_exist(self):
		response = Client().get('/register/')
		self.assertEqual(response.status_code, 200)

	def test_register_using_register_func(self):
		found = resolve('/register/')
		self.assertEqual(found.func, register)

	def test_register_page_using_register_template(self):
		response = Client().get('/register/')
		self.assertTemplateUsed(response, 'register.html')

	def test_register_page_after_user_login(self):
		c = Client()
		user = User.objects.create(username='rifqi')
		user.set_password('rifqi')
		user.save()
		data = {'username':'rifqi', 'password':'rifqi'}

		response = c.post("/", data=data)
		self.assertEquals(response.status_code, 302)

		response2 = c.get('/register/')
		self.assertEquals(response2.status_code, 302)

	def test_form_input_html(self):
		form = Register_Form()
		self.assertIn('id="id_first_name', form.as_p())
		self.assertIn('id="id_last_name', form.as_p())
		self.assertIn('id="id_username', form.as_p())
		self.assertIn('id="id_email', form.as_p())
		self.assertIn('id="id_password1', form.as_p())
		self.assertIn('id="id_password2', form.as_p())

	def test_form_validation_blank(self):
		form = Register_Form(data={'first_name':'', 'last_name':'', 'username':'', 'email':'', 'password1':'', 'password2':''})
		self.assertFalse(form.is_valid())
		self.assertEquals(form.errors['username'], ["This field is required."])
		self.assertEquals(form.errors['email'], ["This field is required."])
		self.assertEquals(form.errors['password1'], ["This field is required."])
		self.assertEquals(form.errors['password2'], ["This field is required."])

	def test_save_register_form(self):
		form = Register_Form(data={'first_name':'kak', 'last_name':'pewe', 'username':'kakpewe', 'email':'kpw@gmail.com', 'password1':'ppwisfun0-', 'password2':'ppwisfun0-'})
		if form.is_valid():
			form.save()
		countUser = User.objects.all().count()
		self.assertEqual(countUser, 1)

	def test_views_if_post(self):
		c = Client()
		data = data={'first_name':'kak', 'last_name':'pewe', 'username':'kakpewe', 'email':'kpw@gmail.com', 'password1':'ppwisfun0-', 'password2':'ppwisfun0-'}
		response = c.post('/register/', data=data)
		self.assertEqual(response.status_code, 302)

	def test_get_register_json(self):
		request = HttpRequest()
		response = check_username(request)
		self.assertTrue(type(response) == JsonResponse)