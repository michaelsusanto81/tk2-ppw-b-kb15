from django.conf.urls import url
from django.urls import path
from . import views

app_name = "register"

urlpatterns = [
    path('', views.register, name='register'),
    path('ajax/check_username/', views.check_username, name='check_username')
]