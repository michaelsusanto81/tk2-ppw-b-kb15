from django.shortcuts import render, redirect
from .forms import Register_Form
from django.contrib.auth.models import User
from django.http import JsonResponse

# Create your views here.
def register(request):
	if request.user.is_authenticated:
		return redirect('/findDrinks/')
	else:
		if request.method == 'POST':
			form = Register_Form(request.POST)
			if form.is_valid():
				form.save()
				return redirect('/')
		else:
			form = Register_Form()

		return render(request, 'register.html', {'form':form})

def check_username(request):
	username = request.GET.get('username', None)
	data = {
		'is_exist': User.objects.filter(username__iexact=username).exists()
	}
	if data['is_exist']:
		data['error_message'] = 'A user with that username already exists'
	return JsonResponse(data);