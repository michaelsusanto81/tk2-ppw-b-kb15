$(document).ready(function(){

	$("#id_username").keyup(function(){
		var username = $(this).val();
		$('#error_msg').css('display', 'none')
		$('#register_btn').attr('disabled', false);

		$.ajax({
			url: '/register/ajax/check_username/',
			data: {
				'username': username
			},
			dataType: 'json',
			success: function(data) {
				$('#error_msg').css('display', 'none')
				$('#id_username').css('background-color', '#FFFFFF')
				if (data.is_exist) {
					$('#id_username').css('background-color', '#FFBCB8')
					$('#error_msg').html(data.error_message)
					$('#error_msg').css('display', 'flex')
					$('#register_btn').attr('disabled', true);
				}
			}
		});
	});

	$("#id_first_name").keyup(function(){
		var name = $(this).val();
		$('#error_msg').css('display', 'none')
		$('#register_btn').attr('disabled', false);
		$('#error_msg').css('display', 'none')
		$('#id_first_name').css('background-color', '#FFFFFF')

		if (name.length == 0) {
			$('#id_first_name').css('background-color', '#FFBCB8')
			$('#error_msg').html("Please enter your name")
			$('#error_msg').css('display', 'flex')
			$('#register_btn').attr('disabled', true);
		}
	});

	$("#id_email").change(function(){
		var email = $(this).val();
		$('#error_msg').css('display', 'none')
		$('#register_btn').attr('disabled', false);
		$('#error_msg').css('display', 'none')
		$('#id_email').css('background-color', '#FFFFFF')

		if ((email.indexOf('@') == -1) || (email.indexOf('@') == email.length-1) || (email.indexOf('.') == -1)) {
			$('#id_email').css('background-color', '#FFBCB8')
			$('#error_msg').html("Please complete your email address")
			$('#error_msg').css('display', 'flex')
			$('#register_btn').attr('disabled', true);
		}
	});

	$("#id_password1").change(function(){
		var password = $(this).val();
		$('#error_msg').css('display', 'none')
		$('#register_btn').attr('disabled', false);
		$('#error_msg').css('display', 'none')
		$('#id_password1').css('background-color', '#FFFFFF')

		if ((password.indexOf(' ') != -1) || (password.length < 8)) {
			$('#id_password1').css('background-color', '#FFBCB8')
			$('#error_msg').html("Password must be at least 8 characters")
			$('#error_msg').css('display', 'flex')
			$('#register_btn').attr('disabled', true);
		}
	});

	$("#id_password2").keyup(function(){
		var password2 = $(this).val();
		var password1 = $('#id_password1').val();
		$('#register_btn').attr('disabled', false);
		$('#id_password2').css('background-color', '#FFFFFF')

		if (password2 !== password1 && password2.length > 0){
			if (password2.length == password1.length) {
				$('#id_password2').css('background-color', '#FFBCB8')
			}
			$('#error_msg').html("Please enter the same password as above")
			$('#error_msg').css('display', 'flex')
			$('#register_btn').attr('disabled', true);
		} else if (password2 === password1) {
			$('#error_msg').css('display', 'none')
		}
	});

});