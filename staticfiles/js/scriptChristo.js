const formatDrinks= (data) => {
    var html=" "
    $('#all-drinks').empty();
    if(data.length === 0){
        html+= "<span class='subtitle'>Please input the drinks first!</span>";
        $('#all-drinks').append(html);
    }else{
        html += '<div class="container"><div class="card-columns">';
        url=''
        for(var i=0; i< data.length;i++){
            html += '<div class="card mx-auto" style="width: 18rem;">';
            html = html + '<img src="https://finddrink.herokuapp.com/media/' + data[i]['fields']['drink_image'] + '" class="card-img-top" alt="...">';
            html += '<div class="card-body">';
            html = html + '<h5 class="card-title">' + data[i]['fields']['drink_name'] + '</h5>';
            html = html + '<p class="card-text">Store: ' + data[i]['fields']['store_name'] + '</p>';
            html = html + '<p class="card-text">Location: ' + data[i]['fields']['store_location'] + '</p>';
            html = html + '<p class="card-text">Price: ' + data[i]['fields']['price'] + '</p>';
            html += '</div></div>';
        }
        html += '<div></div>';
        $('#all-drinks').append(html);
    }
}



$(document).ready(()=> {
    $.ajax({
        method:"GET",
        url: 'getAllDrinks/',
        datatype: 'json',
        success: function(data){
            $('#all-drinks').empty();
            formatDrinks(data);
        }
    });
})
