$(function() {
	$('#id_category').click(function() {
		var keyword = $('#id_category').val();
		$.ajax({
			method: 'GET',
			url: '/getRecommendation/' + keyword,
			dataType: 'json',
			success: function(data) {
				var html = '<h1>Recommendations</h1>'
				$('.recommendations').empty();
				if(data.length === 0) {
					html += '<h3 class="txt-setting">We are sorry, there aren\'t any drinks in your area.</h3>';
					$('.recommendations').append(html);
				} else {
					html += '<div class="container"><div class="card-columns">';
					url = ''
					for(var i = 0; i < data.length; i++) {
						html += '<div class="card mx-auto" style="width: 18rem;">';
						html = html + '<img src="https://finddrink.herokuapp.com/media/' + data[i]['drink_image'] + '" class="card-img-top" alt="...">';
						html += '<div class="card-body">';
						html = html + '<h5 class="card-title">' + data[i]['drink_name'] + '</h5>';
						html = html + '<p class="card-text">Store: ' + data[i]['store_name'] + '</p>';
						html = html + '<p class="card-text">Location: ' + data[i]['store_location'] + '</p>';
						html = html + '<p class="card-text">Price: ' + data[i]['price'] + '</p>';
						html += '</div></div>';
					}
					html += '</div></div>';
					$('.recommendations').append(html);
					// console.log(data);
				}
				// console.log(data);
				// console.log(data.length);
			}
		})
	})

	$('.find-form').mouseenter(function() {
		$('.find-form').css({'transition':'1s', 'border':'5px solid #edfc95'});
	})

	$('.find-form').mouseleave(function() {
		$('.find-form').css({'transition':'1s', 'border': '0'});
	})
})